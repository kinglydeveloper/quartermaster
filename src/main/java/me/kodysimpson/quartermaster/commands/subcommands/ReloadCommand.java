package me.kodysimpson.quartermaster.commands.subcommands;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.commands.CommandManager;
import me.kodysimpson.quartermaster.commands.SubCommand;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ReloadCommand extends SubCommand {

    @Override
    public String getName(){
        return "reload";
    }

    @Override
    public String getDescription(){
        return "Reload the configuration of the plugin";
    }

    @Override
    public String getSyntax(){
        return "/qm reload";
    }

    @Override
    public void perform(Player p, String args[]){

        if (p.hasPermission("qm.admin")){
            //Reload the configuration
            QuarterMaster.getPlugin().reloadConfig();
            QuarterMaster.getPlugin().saveConfig();
        }else{
            p.sendMessage(ChatColor.RED + "Silly rabbit, tricks are for kids.");
            p.sendMessage(ChatColor.RED + "This command is for staff.");
        }

    }

}

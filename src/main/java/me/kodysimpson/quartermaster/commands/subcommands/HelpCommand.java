package me.kodysimpson.quartermaster.commands.subcommands;

import me.kodysimpson.quartermaster.commands.CommandManager;
import me.kodysimpson.quartermaster.commands.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class HelpCommand extends SubCommand {

    @Override
    public String getName(){
        return "help";
    }

    @Override
    public String getDescription(){
        return "Show all of the commands for QuarterMaster";
    }

    @Override
    public String getSyntax(){
        return "/qm help";
    }

    @Override
    public void perform(Player p, String args[]){

        CommandManager commandManager = new CommandManager();
        p.sendMessage(ChatColor.DARK_RED + "======= " + ChatColor.BLUE + ChatColor.BOLD + "Quarter" + ChatColor.RED + ChatColor.ITALIC + "Master " + ChatColor.YELLOW + "Commands " + ChatColor.DARK_RED + "=======");
        for (int i = 0; i < commandManager.getSubCommands().size(); i++){
            p.sendMessage(ChatColor.YELLOW + commandManager.getSubCommands().get(i).getSyntax() + " - " + ChatColor.GRAY + ChatColor.ITALIC + commandManager.getSubCommands().get(i).getDescription());
        }
        p.sendMessage(ChatColor.DARK_RED + "=====================================");

    }

}

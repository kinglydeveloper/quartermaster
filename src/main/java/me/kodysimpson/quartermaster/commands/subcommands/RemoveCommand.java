package me.kodysimpson.quartermaster.commands.subcommands;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.commands.SubCommand;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.menu.standard.RemoveLockMenu;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class RemoveCommand extends SubCommand {


    @Override
    public String getName() {
        return "remove";
    }

    @Override
    public String getDescription() {
        return "Remove the lock you are looking at";
    }

    @Override
    public String getSyntax() {
        return "/qm remove";
    }

    @Override
    public void perform(Player p, String[] args) {
        Block target;

        //if (p.hasPermission("qm.remove")){

        //Get the player's LockManagerMenu
        PlayerMenuUtility playerMenuUtility = QuarterMaster.getPlayerMenuUtility(p);

        //See if the user is in a lockable-world(config.yml worlds)
        if (LockUtils.getLockableWorlds().contains(p.getWorld().getName())) {
            if (!(p.getTargetBlockExact(5) == null)) {

                target = p.getTargetBlockExact(5);

                if (LockUtils.getLockableBlocks().contains(target.getType().toString())) {
                    if (LockUtils.isCurrentlyLocked(target)) {
                        if (LockUtils.getWhoLocked(target).equals(p)) { //if the lock is owned by you
                            p.sendMessage(ChatColor.GRAY + "Remove lock?");

                            playerMenuUtility.setLockToRemove(LockUtils.getLock(target.getLocation()));
                            new RemoveLockMenu().open(p);

                        }else if(p.hasPermission("qm.admin")){ //see if an admin is running the command
                            p.sendMessage(ChatColor.GRAY + "Remove player lock?");

                            playerMenuUtility.setLockToRemove(LockUtils.getConnectedLockFromDoorHalf(target));
                            new RemoveLockMenu().open(p);

                        } else {
                            p.sendMessage(ChatColor.RED + "That lock is owned by " + ChatColor.YELLOW + LockUtils.getWhoLocked(target).getName() + ChatColor.RED + ".");
                            p.sendMessage(ChatColor.RED + "You cannot remove it.");
                        }
                    } else {
                        //not currently locked, see if its a chest connected to a locked chest(Double chest)
                        if (LockUtils.isConnectedLockedChest(target)) {
                            if (LockUtils.getOwnerConnectedChest(target).equals(p)) { //The person who ran the command owns the double chest lock

                                playerMenuUtility.setLockToRemove(LockUtils.getLock(LockUtils.getConnectedLocation(target)));
                                new RemoveLockMenu().open(p);

                            }  else {
                                p.sendMessage(ChatColor.RED + "That lock is owned by " + ChatColor.YELLOW + LockUtils.getOwnerConnectedChest(target).getName() + ChatColor.RED + ".");
                                p.sendMessage(ChatColor.RED + "You cannot remove it.");
                            }
                        }else if(LockUtils.isConnectedLockedDoorHalf(target)){

                            //See if the owner of the door half is running the command
                            if (LockUtils.getWhoLocked(LockUtils.getConnectedLockFromDoorHalf(target).getLockID()).equals(p)){
                                p.sendMessage(ChatColor.GRAY + "Remove lock?");

                                playerMenuUtility.setLockToRemove(LockUtils.getConnectedLockFromDoorHalf(target));
                                new RemoveLockMenu().open(p);
                            }else if(p.hasPermission("qm.admin")){ //see if an admin is running the command
                                p.sendMessage(ChatColor.GRAY + "Remove player lock?");

                                playerMenuUtility.setLockToRemove(LockUtils.getConnectedLockFromDoorHalf(target));
                                new RemoveLockMenu().open(p);

                            }else{ //A random person is running the command
                                p.sendMessage(ChatColor.RED + "That lock is owned by " + ChatColor.YELLOW + LockUtils.getWhoLocked(LockUtils.getConnectedLockFromDoorHalf(target).getLockID()).getName() + ChatColor.RED + ".");
                                p.sendMessage(ChatColor.RED + "You cannot remove it.");
                            }


                        } else {
                            p.sendMessage(ChatColor.GREEN + "This block is not locked, there is nothing to remove.");
                        }

                    }
                } else {
                    p.sendMessage(ChatColor.RED + "This block can not be locked.");
                }
            } else if (p.getTargetBlockExact(5) == null) {
                p.sendMessage(ChatColor.GRAY + "Look at something nearby.");
            }
        } else {
            p.sendMessage(ChatColor.RED + "You can not make locks in this world.");
        }
    }


}

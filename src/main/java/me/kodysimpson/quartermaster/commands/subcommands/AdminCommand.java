package me.kodysimpson.quartermaster.commands.subcommands;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.commands.SubCommand;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.menu.admin.PlayersWithLocksAdminMenu;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class AdminCommand extends SubCommand {


    @Override
    public String getName() {
        return "admin";
    }

    @Override
    public String getDescription() {
        return "Manage player settings and locks";
    }

    @Override
    public String getSyntax() {
        return "/qm admin";
    }

    @Override
    public void perform(Player p, String[] args) {

        if (p.hasPermission("qm.admin")) {
            //Display all of the players who own locks
            PlayerMenuUtility playerMenuUtility = QuarterMaster.getPlayerMenuUtility(p);

            new PlayersWithLocksAdminMenu().open(p);
        } else {
            p.sendMessage(ChatColor.RED + "Silly rabbit, tricks are for kids.");
            p.sendMessage(ChatColor.RED + "This command is for admins.");
        }

    }
}

package me.kodysimpson.quartermaster.model;

import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

public class Lock {

    private String lockID;
    private OfflinePlayer owner;
    private String blockType;
    private Location location;
    private Timestamp creationDate;
    private ArrayList<OfflinePlayer> playersWithAccess;

    public Lock(String lockID, String owner, String blockType, Location location, Timestamp creationDate) {
        this.lockID = lockID;
        this.owner = Bukkit.getOfflinePlayer(UUID.fromString(owner));
        this.blockType = blockType;
        this.location = location;
        this.creationDate = creationDate;
        this.playersWithAccess = (ArrayList<OfflinePlayer>) LockUtils.getAccessListFromID(this.lockID);
    }

    public ArrayList<OfflinePlayer> getPlayersWithAccess() {
        return playersWithAccess;
    }

    public String getLockID() {
        return lockID;
    }

    public OfflinePlayer getOwner() {
        return owner;
    }

    public String getBlockType() {
        return blockType;
    }

    public Location getLocation() {
        return location;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

}

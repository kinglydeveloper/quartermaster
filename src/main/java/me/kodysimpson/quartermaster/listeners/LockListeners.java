package me.kodysimpson.quartermaster.listeners;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.model.Lock;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.data.type.Door;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.UUID;

public class LockListeners implements Listener {

    //Prevent someone from opening or using the block
    // if they do not own it or have been granted access
    @EventHandler
    public void openChestListener(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Block b = e.getClickedBlock();
            if (LockUtils.getLockableBlocks().contains(b.getType().toString())) {
                System.out.println("This is a lockable block");
                //check to see if anyone has locked this chest
                if (LockUtils.isCurrentlyLocked(b)) {
                    System.out.println("Trying to open a locked block");
                    if (LockUtils.getWhoLocked(b) == e.getPlayer()) {
                        e.getPlayer().sendMessage(ChatColor.GREEN + "You are opening one of your locks.");
                    } else if (LockUtils.getAccessListFromBlock(b).contains(e.getPlayer().getUniqueId().toString())) { //See if the person trying to open the lock is otherwise on the access list
                        e.getPlayer().sendMessage(ChatColor.GREEN + "You are opening a " + b.getType().toString().toLowerCase() + " owned by " + ChatColor.GRAY + LockUtils.getWhoLocked(b).getName());
                    } else if (!(LockUtils.getWhoLocked(b) == e.getPlayer())) {
                        if (e.getPlayer().hasPermission("qm.admin")) {
                            e.getPlayer().sendMessage(ChatColor.GRAY + "Opening lock as admin");
                        } else {
                            e.setCancelled(true);
                            e.getPlayer().sendMessage(ChatColor.RED + "This " + b.getType().toString().toLowerCase() + " has been locked by " + ChatColor.GRAY + LockUtils.getWhoLocked(b).getName());
                            e.getPlayer().sendMessage(ChatColor.RED + "If you wish to use this, request the owner for access.");
                        }
                    }
                } else { //block may not be locked, but could be connected a lock that is
                    System.out.println("Trying to open a not locked block");
                    //See if it's a door
                    if ((b.getState().getBlockData() instanceof Door) && LockUtils.getLockableBlocks().contains(b.getType().toString())){


                        Door door = (Door) b.getState().getBlockData();

                        Location newLocation = null;
                        if (door.getHalf().toString().equalsIgnoreCase("bottom")){

                            newLocation = b.getLocation().add(0, 1, 0);

                        }else if(door.getHalf().toString().equalsIgnoreCase("top")){

                            newLocation = b.getLocation().add(0, -1, 0);

                        }

                        if (LockUtils.isCurrentlyLocked(newLocation)){ //see if the location of the opposite half of the door is locked

                            ArrayList<OfflinePlayer> accessList = (ArrayList<OfflinePlayer>) LockUtils.getAccessListFromLocation(newLocation);

                            OfflinePlayer owner = LockUtils.getWhoLocked(newLocation);

                            if (owner == e.getPlayer()) {
                                e.getPlayer().sendMessage(ChatColor.GREEN + "You are opening one of your locks.");
                            } else if (accessList.contains(e.getPlayer())) { //See if the person trying to open the lock is otherwise on the access list
                                e.getPlayer().sendMessage(ChatColor.GREEN + "You are opening a " + b.getType().toString().toLowerCase() + " owned by " + ChatColor.GRAY + owner.getName());
                            } else if (!(owner == e.getPlayer())) {
                                if (e.getPlayer().hasPermission("qm.admin")) {
                                    e.getPlayer().sendMessage(ChatColor.GRAY + "Opening lock as admin");
                                } else {
                                    e.setCancelled(true);
                                    e.getPlayer().sendMessage(ChatColor.RED + "This " + b.getType().toString().toLowerCase() + " has been locked by " + ChatColor.GRAY + owner.getName());
                                    e.getPlayer().sendMessage(ChatColor.RED + "If you wish to use this, request the owner for access.");
                                }
                            }


                        }

                    }


                    if (LockUtils.isConnectedLockedChest(b) && (LockUtils.getLockableBlocks().contains(b.getType().toString()))) {
                        OfflinePlayer owner = LockUtils.getOwnerConnectedChest(b);
                        if (owner == e.getPlayer()) {
                            e.getPlayer().sendMessage(ChatColor.GREEN + "You are opening one of your locks.");
                        } else if (LockUtils.getAccessListFromConnectedBlock(b).contains(e.getPlayer().getUniqueId().toString())) { //See if the person trying to open the lock is otherwise on the access list
                            e.getPlayer().sendMessage(ChatColor.GREEN + "You are opening a " + b.getType().toString().toLowerCase() + " owned by " + ChatColor.GRAY + owner.getName());
                        } else if (!(owner == e.getPlayer())) {
                            if (e.getPlayer().hasPermission("qm.admin")) {
                                e.getPlayer().sendMessage(ChatColor.GRAY + "Opening lock as admin");
                            } else {
                                e.setCancelled(true);
                                e.getPlayer().sendMessage(ChatColor.RED + "This " + b.getType().toString().toLowerCase() + " has been locked by " + ChatColor.GRAY + owner.getName());
                                e.getPlayer().sendMessage(ChatColor.RED + "If you wish to use this, request the owner for access.");
                            }
                        }
                    }
                }
            }
        }
    }

    //Prevent the users from breaking locks that aren't theirs
    @EventHandler
    public void breakChestListener(BlockBreakEvent e) {
        Block block = e.getBlock();
        if (LockUtils.getLockableBlocks().contains(e.getBlock().getType().toString())) { //see if this is a lockable block
            if (LockUtils.isCurrentlyLocked(e.getBlock())) { //see if this lockable block is already locked
                if (e.getPlayer().equals(LockUtils.getWhoLocked(e.getBlock()))) {
                    //Since the chest is broken, remove lock from database
                    if (block.getType().equals(Material.CHEST) || block.getType().equals(Material.TRAPPED_CHEST)) {
                        BlockState state = block.getState();
                        Chest chest = (Chest) state;
                        Inventory inventory = chest.getInventory();
                        if (inventory instanceof DoubleChestInventory) {
                            //use the location found to alter the lock in the DB
                            LockUtils.alterLockLocation(block, LockUtils.getConnectedLocation(block));
                        }else{
                            LockUtils.deleteLock(e.getBlock());
                            e.getPlayer().sendMessage(ChatColor.GRAY + "The lock associated with this block has been deleted.");
                        }
                    }else {
                        LockUtils.deleteLock(e.getBlock());
                        e.getPlayer().sendMessage(ChatColor.GRAY + "The lock associated with this block has been deleted.");
                    }
                } else{
                    e.getPlayer().sendMessage(ChatColor.RED + "This " + e.getBlock().getType().toString().toLowerCase() + " has been locked by " + ChatColor.GRAY + LockUtils.getWhoLocked(e.getBlock()).getName());
                    e.getPlayer().sendMessage(ChatColor.RED + "You cannot break it.");

                    e.setCancelled(true);
                }
            } else {

                //See if it's a door
                if ((e.getBlock().getState().getBlockData() instanceof Door) && LockUtils.getLockableBlocks().contains(e.getBlock().getType().toString())){


                    final Door door = (Door) e.getBlock().getState().getBlockData();

                    Location newLocation = null;
                    if (door.getHalf().toString().equalsIgnoreCase("bottom")){

                        newLocation = e.getBlock().getLocation().add(0, 1, 0);

                    }else if(door.getHalf().toString().equalsIgnoreCase("top")){

                        newLocation = e.getBlock().getLocation().add(0, -1, 0);

                    }

                    if (LockUtils.isCurrentlyLocked(newLocation)){ //see if the location of the opposite half of the door is locked

                        final Lock lock = LockUtils.getLock(newLocation);

                        if (e.getPlayer().equals(lock.getOwner())){
                            LockUtils.deleteLock(lock.getLockID());
                            e.getPlayer().sendMessage(ChatColor.GRAY + "The lock associated with this block has been deleted.");
                        }else{
                            e.setCancelled(true);
                            e.getPlayer().sendMessage(ChatColor.RED + "This block has been locked by " + ChatColor.GRAY + lock.getOwner().getName());
                            e.getPlayer().sendMessage(ChatColor.RED + "You cannot break it.");
                        }


                    }

                }else if (LockUtils.isConnectedLockedChest(block)){ //this block is not a lock but is connected to a chest that is a lock

                    if (!LockUtils.getWhoLocked(LockUtils.getConnectedLocation(block)).equals(e.getPlayer())){
                        e.getPlayer().sendMessage(ChatColor.RED + "This chest has been locked by " + ChatColor.GRAY + LockUtils.getWhoLocked(LockUtils.getConnectedLocation(block)).getName());
                        e.getPlayer().sendMessage(ChatColor.RED + "You cannot break it.");
                        e.setCancelled(true);
                    }

                }
            }
        }else{ //see if there is a door above this block
            if (e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ()).getState().getBlockData() instanceof Door){

                //See if that bottom door is locked or not
                if (LockUtils.isCurrentlyLocked(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ())) || LockUtils.isCurrentlyLocked(Bukkit.getWorld("world").getBlockAt(block.getX(), block.getY() + 2, block.getZ()))){

                    Lock lock;

                    //figure out which location of the door represents the lock,
                    // and then see if the person trying to destroy the block under the door
                    //owns the lock or not
                    if (LockUtils.isCurrentlyLocked(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ()))){

                        lock = LockUtils.getLock(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ()).getLocation());

                        System.out.println(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ()).getLocation().getBlockX() + " " + Bukkit.getWorld("world").getBlockAt(block.getX(), block.getY() + 1, block.getZ()).getLocation().getBlockY());

                        //the bottom is locked
                        if (e.getPlayer().equals(LockUtils.getWhoLocked(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ())))){

                            e.getPlayer().sendMessage(ChatColor.GRAY + "The lock associated with this block has been deleted.");

                            LockUtils.deleteLock(lock.getLockID());
                        }else{

                            e.getPlayer().sendMessage(ChatColor.RED + "The door above this block has been locked by " + ChatColor.GRAY + lock.getOwner().getName());
                            e.getPlayer().sendMessage(ChatColor.RED + "You cannot break it.");

                            e.setCancelled(true);
                        }
                    }else if(LockUtils.isCurrentlyLocked(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 2, block.getZ()))){

                        lock = LockUtils.getLock(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 2, block.getZ()).getLocation());
                        System.out.println(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ()).getLocation().getBlockX() + " " + Bukkit.getWorld("world").getBlockAt(block.getX(), block.getY() + 1, block.getZ()).getLocation().getBlockY());

                        //the top of the door is locked
                        if (e.getPlayer().equals(LockUtils.getWhoLocked(e.getPlayer().getWorld().getBlockAt(block.getX(), block.getY() + 2, block.getZ())))){

                            e.getPlayer().sendMessage(ChatColor.GRAY + "The lock associated with this block has been deleted.");

                            LockUtils.deleteLock(lock.getLockID());
                        }else{

                            e.getPlayer().sendMessage(ChatColor.RED + "The door above this block has been locked by " + ChatColor.GRAY + lock.getOwner().getName());
                            e.getPlayer().sendMessage(ChatColor.RED + "You cannot break it.");

                            e.setCancelled(true);
                        }
                    }

                }

            }

        }
    }

    //Add the following safety so that raiders cant "hack" their way into a lock

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void blockExplosionEvent(EntityExplodeEvent e) {
        //loop through all of the blocks broken
        for (int i = 0; i < e.blockList().size(); i++) {
            if (LockUtils.getLockableBlocks().contains(e.blockList().get(i).getType().toString())){
                if (LockUtils.isCurrentlyLocked(e.blockList().get(i))) { //this block is a lock
                    if (QuarterMaster.getPlugin().getConfig().getBoolean("raid-safety")) {

                        e.blockList().remove(i);

                    }else {

                        //See if the lock has another chest connected to it to transfer ownership to
                        if (e.blockList().get(i).getType().equals(Material.CHEST) || e.blockList().get(i).getType().equals(Material.TRAPPED_CHEST)) {

                            BlockState state = e.blockList().get(i).getState();
                            Chest chest = (Chest) state;
                            Inventory inventory = chest.getInventory();
                            if (inventory instanceof DoubleChestInventory) {
                                //use the location found to alter the lock in the DB
                                LockUtils.alterLockLocation(e.blockList().get(i), LockUtils.getConnectedLocation(e.blockList().get(i)));
                            }
                        } else {
                            LockUtils.deleteLock(e.blockList().get(i));
                        }
                    }
                } else if (LockUtils.isConnectedLockedChest(e.blockList().get(i))) { //this block is not a lock but is connected to a chest that is a lock

                    if (QuarterMaster.getPlugin().getConfig().getBoolean("raid-safety")) {
                        e.blockList().remove(i);
                    } else {
                        LockUtils.deleteLock(e.blockList().get(i));
                    }
                }else{

                    //See if it's a door
                    if (e.blockList().get(i).getState().getBlockData() instanceof Door){


                        Door door = (Door) e.blockList().get(i).getState().getBlockData();

                        Location newLocation = null;
                        if (door.getHalf().toString().equalsIgnoreCase("bottom")){

                            newLocation = e.blockList().get(i).getLocation().add(0, 1, 0);

                        }else if(door.getHalf().toString().equalsIgnoreCase("top")){

                            newLocation = e.blockList().get(i).getLocation().add(0, -1, 0);

                        }

                        //Get the location of the door half that IS locked

                        if (LockUtils.isCurrentlyLocked(newLocation)){ //see if the location of the opposite half of the door is locked

                            if (QuarterMaster.getPlugin().getConfig().getBoolean("raid-safety")) {
                                e.blockList().remove(i);
                            } else {
                                LockUtils.deleteLock(e.blockList().get(i));
                            }

                        }

                    }

                }
            }else if (e.getLocation().getWorld().getBlockAt(e.blockList().get(i).getX(), e.blockList().get(i).getY() + 1, e.blockList().get(i).getZ()).getState().getBlockData() instanceof Door) {

                //See if either halfs of the above door are locked
                if (LockUtils.isCurrentlyLocked(e.getLocation().getWorld().getBlockAt(e.blockList().get(i).getX(), e.blockList().get(i).getY() + 1, e.blockList().get(i).getZ())) || LockUtils.isCurrentlyLocked(e.getLocation().getWorld().getBlockAt(e.blockList().get(i).getX(), e.blockList().get(i).getY() + 2, e.blockList().get(i).getZ()))) {
                    if (QuarterMaster.getPlugin().getConfig().getBoolean("raid-safety")) {
                        e.blockList().remove(i);
                    } else {  //Remove this "underblock" from the exploded blocks list since there is a locked door above it
                        LockUtils.deleteLock(e.blockList().get(i));
                    }

                }
            }
        }
    }
}

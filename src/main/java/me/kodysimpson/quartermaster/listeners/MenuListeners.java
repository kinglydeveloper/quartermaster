package me.kodysimpson.quartermaster.listeners;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;


public class MenuListeners implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        //Make sure the player has a menu system object
        QuarterMaster.getPlayerMenuUtility(p);
        //Get the player's LockManagerMenu
        PlayerMenuUtility playerMenuUtility = QuarterMaster.lockManagerMenuList.get(p);

        InventoryHolder holder = e.getInventory().getHolder();
        if (holder instanceof Menu) {
            e.setCancelled(true);
            if (e.getCurrentItem() == null) {
                return;
            }
            Menu menu = (Menu) holder;
            menu.handleMenu(e, playerMenuUtility);
        }

    }

}

package me.kodysimpson.quartermaster.listeners;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.utils.PlayerSettingsUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinLeaveListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){

        Player p = e.getPlayer();

        //Every time a player joins, assign them a personal LockManagerMenu object
        QuarterMaster.getPlayerMenuUtility(p);

        //Create a settings profile for the player if they don't have one already
        PlayerSettingsUtils.createPlayerSettings(p.getUniqueId().toString());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){

        Player p = e.getPlayer();
        //Since the player left, get rid of the LockManagerMenu object for that player
        if (QuarterMaster.lockManagerMenuList.containsKey(p)){
            QuarterMaster.lockManagerMenuList.remove(p);
        }

    }

}

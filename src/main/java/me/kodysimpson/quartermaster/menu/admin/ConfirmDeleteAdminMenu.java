package me.kodysimpson.quartermaster.menu.admin;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ConfirmDeleteAdminMenu extends Menu {

    @Override
    public String getMenuName() {
        return "Admin Confirm: Delete Lock?";
    }

    @Override
    public int getSlots() {
        return 9;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();

        if (e.getCurrentItem().getType().equals(Material.EMERALD)) {

            OfflinePlayer lockOwner = LockUtils.getWhoLocked(playerMenuUtility.getLockID());

            //They clicked yes, so delete lock
            LockUtils.deleteLock(playerMenuUtility.getLockID());

            if (lockOwner.isOnline()) {
                lockOwner.getPlayer().sendMessage(ChatColor.GREEN + "Your lock(" + ChatColor.GOLD + playerMenuUtility.getLockID() + ChatColor.GREEN + ") has been deleted by an admin.");
            }

            p.sendMessage(ChatColor.GREEN + "Lock Deleted.");

            p.closeInventory();
        } else if (e.getCurrentItem().equals(Material.BARRIER)) {
            //Selected no, so just go back
            new ManageLockAdminMenu().open(p);
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        ItemStack yes = new ItemStack(Material.EMERALD, 1);
        ItemMeta yes_meta = yes.getItemMeta();
        yes_meta.setDisplayName(ChatColor.GREEN + "Yes");
        yes.setItemMeta(yes_meta);
        ItemStack no = new ItemStack(Material.BARRIER, 1);
        ItemMeta no_meta = no.getItemMeta();
        no_meta.setDisplayName(ChatColor.DARK_RED + "No");
        no.setItemMeta(no_meta);

        inventory.setItem(3, yes);
        inventory.setItem(5, no);

        setFillerGlass();
    }
}

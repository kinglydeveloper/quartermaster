package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import me.kodysimpson.quartermaster.utils.PlayerSettingsUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class AddPlayerMenu extends Menu {


    @Override
    public String getMenuName() {
        return "Choose a Player to Add";
    }

    @Override
    public int getSlots() {
        return 54;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        if (e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {

            //See if unlimited friends is set in the config.yml
            if (PlayerSettingsUtils.hasUnlimitedFriends()) {
                //Get player chosen
                Player playerToAdd = Bukkit.getPlayer(e.getClickedInventory().getItem(e.getSlot()).getItemMeta().getDisplayName());

                playerMenuUtility.setPlayerToAdd(playerToAdd);

                //Now that they have chosen a player, let's verify that selection
                new ConfirmAddMenu().open(p);
            } else if (PlayerSettingsUtils.getSetting("unlimitedFriends", Bukkit.getPlayer(e.getClickedInventory().getItem(e.getSlot()).getItemMeta().getDisplayName()).getUniqueId().toString())) {//Get player chosen
                Player playerToAdd = Bukkit.getPlayer(e.getClickedInventory().getItem(e.getSlot()).getItemMeta().getDisplayName());

                playerMenuUtility.setPlayerToAdd(playerToAdd);

                //Now that they have chosen a player, let's verify that selection
                new ConfirmAddMenu().open(p);
            } else if (LockUtils.getAccessListFromID(playerMenuUtility.getLockID()).size() < QuarterMaster.getPlugin().getConfig().getInt("max-friends")) {
                //see if the amount of friends on this lock is less than the max per lock
                //it is, so continue
                //Get player chosen
                Player playerToAdd = Bukkit.getPlayer(e.getClickedInventory().getItem(e.getSlot()).getItemMeta().getDisplayName());

                playerMenuUtility.setPlayerToAdd(playerToAdd);

                //Now that they have chosen a player, let's verify that selection
                new ConfirmAddMenu().open(p);
            } else {
                p.sendMessage(ChatColor.RED + "You cannot add any more friends to the lock. The max is " + ChatColor.YELLOW + QuarterMaster.getPlugin().getConfig().getInt("max-friends"));
            }
        } else if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            new AccessManagerMenu().open(p);
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        Player p = playerMenuUtility.getP();
        ArrayList<OfflinePlayer> accessList = (ArrayList<OfflinePlayer>) LockUtils.getAccessListFromID(playerMenuUtility.getLockID());

        ArrayList<Player> list = new ArrayList<Player>(p.getServer().getOnlinePlayers());
        for (int i = 0; i < list.size(); i++) {
            //Dont put someone who already is on the list or is the owner of the lock
            if ((accessList.contains(list.get(i)) || (LockUtils.getWhoLocked(playerMenuUtility.getLockID()).equals(list.get(i))))){
            }else{
                ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD, 1);
                ItemMeta meta = playerHead.getItemMeta();
                //Set player info on the item
                meta.setDisplayName(list.get(i).getName());
                playerHead.setItemMeta(meta);

                inventory.addItem(playerHead);
            }
        }

        ItemStack close = new ItemStack(Material.BARRIER, 1);
        ItemMeta close_meta = close.getItemMeta();
        close_meta.setDisplayName(ChatColor.DARK_RED + "Close");
        close.setItemMeta(close_meta);

        inventory.setItem(53, close);

        setFillerGlass();
    }
}

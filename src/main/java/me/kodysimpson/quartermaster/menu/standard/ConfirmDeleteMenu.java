package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ConfirmDeleteMenu extends Menu {

    @Override
    public String getMenuName() {
        return "Confirm: Delete Lock?";
    }

    @Override
    public int getSlots() {
        return 9;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();

        if (e.getCurrentItem().getType().equals(Material.EMERALD)) {

            //They clicked yes, so delete lock
            LockUtils.deleteLock(playerMenuUtility.getLockID());
            p.sendMessage(ChatColor.GREEN + "Your lock(" + ChatColor.GOLD + playerMenuUtility.getLockID() + ChatColor.GREEN + ") has been deleted.");

            p.closeInventory();
        } else if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            //Selected no, so just go back
            new ManageLockMenu().open(p);
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        ItemStack yes = new ItemStack(Material.EMERALD, 1);
        ItemMeta yes_meta = yes.getItemMeta();
        yes_meta.setDisplayName(ChatColor.GREEN + "Yes");
        yes.setItemMeta(yes_meta);
        ItemStack no = new ItemStack(Material.BARRIER, 1);
        ItemMeta no_meta = no.getItemMeta();
        no_meta.setDisplayName(ChatColor.DARK_RED + "No");
        no.setItemMeta(no_meta);

        inventory.setItem(3, yes);
        inventory.setItem(5, no);

        setFillerGlass();
    }
}

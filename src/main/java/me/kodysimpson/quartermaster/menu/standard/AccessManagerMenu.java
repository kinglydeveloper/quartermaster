package me.kodysimpson.quartermaster.menu.standard;

import com.google.gson.internal.$Gson$Preconditions;
import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.menu.admin.LocksListAdminMenu;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;

public class AccessManagerMenu extends Menu {

    @Override
    public String getMenuName() {
        return "QM > Access Manager";
    }

    @Override
    public int getSlots() {
        return 45;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();

        if (e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {
            //Show all of the players with the lock
            new PlayersWithAccessMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.ENDER_EYE)) {
            //Show menu for adding a player
            //Ask the user to choose a player
            new AddPlayerMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.REDSTONE_BLOCK)) {
            //Show menu for removing a player
            //Ask the user to choose a player
            new PlayersToRemoveMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            p.closeInventory();
            if (e.getCurrentItem().getItemMeta().getPersistentDataContainer().has(new NamespacedKey(QuarterMaster.getPlugin(), "isAdmin"), PersistentDataType.INTEGER)){
                new LocksListAdminMenu().open(p);
            }else{
                new LocksListMenu().open(p);
            }
        }

    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility){
        ItemStack remove = new ItemStack(Material.REDSTONE_BLOCK, 1);
        ItemMeta remove_meta = remove.getItemMeta();
        remove_meta.setDisplayName(ChatColor.DARK_RED + "Remove Player");
        ArrayList<String> remove_lore = new ArrayList<>();
        remove_lore.add(ChatColor.YELLOW + "Remove players from this lock");
        remove_meta.setLore(remove_lore);
        remove.setItemMeta(remove_meta);

        inventory.setItem(13, remove);

        ItemStack players = new ItemStack(Material.PLAYER_HEAD, 1);
        ItemMeta players_meta = players.getItemMeta();
        players_meta.setDisplayName(ChatColor.AQUA + "View Players");
        ArrayList<String> players_lore = new ArrayList<>();
        players_lore.add(ChatColor.GREEN + "See who has access to your lock");
        players_meta.setLore(players_lore);
        players.setItemMeta(players_meta);

        inventory.setItem(22, players);

        ItemStack add = new ItemStack(Material.ENDER_EYE, 1);
        ItemMeta add_meta = add.getItemMeta();
        add_meta.setDisplayName(ChatColor.GOLD + "Add Player to Lock");
        add.setItemMeta(add_meta);

        inventory.setItem(31, add);

        ItemStack close = new ItemStack(Material.BARRIER, 1);
        ItemMeta close_meta = close.getItemMeta();
        close_meta.setDisplayName(ChatColor.DARK_RED + "Close");

        close_meta.getPersistentDataContainer().set(new NamespacedKey(QuarterMaster.getPlugin(), "isAdmin"), PersistentDataType.INTEGER, 1);

        close.setItemMeta(close_meta);

        inventory.setItem(44, close);

        setFillerGlass();
    }
}

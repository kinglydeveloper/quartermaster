package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ConfirmRemovePlayerMenu extends Menu {

    @Override
    public String getMenuName() {
        return "Confirm: Remove Player";
    }

    @Override
    public int getSlots() {
        return 9;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            new AccessManagerMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.EMERALD)) {
            //They clicked yes, so remove player from this lock's access

            //Take the array from the lock in the DB and make an arraylist
            ArrayList<OfflinePlayer> accessList = (ArrayList<OfflinePlayer>) LockUtils.getAccessListFromID(playerMenuUtility.getLockID());
            if (accessList.contains(Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToRemove()))) {
                LockUtils.removePlayerFromLock(playerMenuUtility.getLockID(), Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToRemove()));
                p.sendMessage(ChatColor.GRAY + "Removed " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToRemove()).getName() + ChatColor.GRAY + " from the lock.");

                if (!(p.equals(LockUtils.getWhoLocked(playerMenuUtility.getLockID())))){
                    if (LockUtils.getWhoLocked(playerMenuUtility.getLockID()).isOnline()){
                        LockUtils.getWhoLocked(playerMenuUtility.getLockID()).getPlayer().sendMessage(ChatColor.YELLOW + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToRemove()).getName() + " has been removed from one of your locks.");
                    }
                }

                if (Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToRemove()).isOnline()){
                    Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToRemove()).getPlayer().sendMessage(ChatColor.YELLOW + "You have been removed from one of " + LockUtils.getWhoLocked(playerMenuUtility.getLockID()).getName() + "'s locks.");
                }

                new AccessManagerMenu().open(p);
            }
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        ItemStack yes = new ItemStack(Material.EMERALD, 1);
        ItemMeta yes_meta = yes.getItemMeta();
        yes_meta.setDisplayName(ChatColor.GREEN + "Yes");
        ArrayList<String> yes_lore = new ArrayList<>();
        yes_lore.add(ChatColor.AQUA + "Remove this player ");
        yes_lore.add(ChatColor.AQUA + "from the lock");
        yes_meta.setLore(yes_lore);
        yes.setItemMeta(yes_meta);
        ItemStack no = new ItemStack(Material.BARRIER, 1);
        ItemMeta no_meta = no.getItemMeta();
        no_meta.setDisplayName(ChatColor.DARK_RED + "No");
        no.setItemMeta(no_meta);

        inventory.setItem(3, yes);
        inventory.setItem(5, no);

        setFillerGlass();
    }
}

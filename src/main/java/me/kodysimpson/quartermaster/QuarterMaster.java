package me.kodysimpson.quartermaster;

import me.kodysimpson.quartermaster.commands.CommandManager;
import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.listeners.MenuListeners;
import me.kodysimpson.quartermaster.listeners.LockListeners;
import me.kodysimpson.quartermaster.listeners.PlayerJoinLeaveListener;
import me.kodysimpson.quartermaster.menu.admin.*;
import me.kodysimpson.quartermaster.menu.standard.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public final class QuarterMaster extends JavaPlugin {

    private static QuarterMaster plugin;

    private static ArrayList<Menu> LIST_OF_MENUS;
    public static HashMap<Player, PlayerMenuUtility> lockManagerMenuList = new HashMap<>();
    private static Connection connection;

    @Override
    public void onEnable() {
        plugin = this;

        //Setup Config
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        //Connect to and initialize the sql database
        try {
            Class.forName("org.h2.Driver");

            String url = "jdbc:h2:" + getDataFolder().getAbsolutePath() + "/data/quartermaster";

            try {
                connection = DriverManager.getConnection(url);

                //Create the desired tables for our database if they don't exist
                Statement statement = connection.createStatement();
                //Table for storing all of the locks
                statement.execute("CREATE TABLE IF NOT EXISTS Locks(LockID int NOT NULL IDENTITY(1, 1), PlayerUUID varchar(255), BlockType varchar(255), X int, Y int, Z int, WorldName varchar(255), CreationDate timestamp);");
                //Table for storing all of people who have access to specific locks
                statement.execute("CREATE TABLE IF NOT EXISTS Access(LockID int NOT NULL, PlayerUUID varchar(255));");
                //Table for storing player settings
                statement.execute("CREATE TABLE IF NOT EXISTS PlayerSettings(PlayerUUID varchar(255), CanMakeLocks boolean, UnlimitedLocks boolean, UnlimitedFriends boolean);");

            } catch (SQLException e) {
                System.out.println("Unable to establish a connection with the database");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Unable to find the h2 DB sql driver");
        }

        //Register the command manager
        getCommand("quartermaster").setExecutor(new CommandManager());

        getServer().getPluginManager().registerEvents(new LockListeners(), this);
        getServer().getPluginManager().registerEvents(new MenuListeners(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinLeaveListener(), this);
    }

    @Override
    public void onDisable() {

        try {
            getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static ArrayList<Menu> getListOfMenus() {
        return LIST_OF_MENUS;
    }

    public static QuarterMaster getPlugin() {
        return plugin;
    }

    public static Connection getConnection() {
        return connection;
    }

    //Provide a player and return a menu system for that player
    //create one if they don't already have one
    public static PlayerMenuUtility getPlayerMenuUtility(Player p) {
        PlayerMenuUtility playerMenuUtility = null;
        if (!(QuarterMaster.lockManagerMenuList.containsKey(p))) { //See if the player has a lockmenusystem "saved" for them

            //This player doesn't. Make one for them add add it to the hashmap
            playerMenuUtility = new PlayerMenuUtility(p);
            QuarterMaster.lockManagerMenuList.put(p, playerMenuUtility);

            return playerMenuUtility;
        } else {
            return QuarterMaster.lockManagerMenuList.get(p); //Return the object by using the provided player
        }
    }
}
